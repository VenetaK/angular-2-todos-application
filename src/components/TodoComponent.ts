import { Component, OnInit } from '@angular/core';
import TodosService from './../services/TodosService';
import Todo from './../models/Todo';

@Component({
  selector: 'todo',
  templateUrl: './src/templates/todos.html'
})

class TodoComponent implements OnInit {
  todosService: TodosService;
  todos: Array<Todo>;
  todoDecs: string;
  constructor() {
    this.todosService = new TodosService();
  }

  getTodos() {
    this.todosService.getTodos().then(todos => {
      this.todos = todos;

      console.log('---load', this.todos);
    });
  }

  ngOnInit() {
    this.getTodos();
  }

  toggleEdit (todo: Todo) {
    todo.inEdit = todo.inEdit?false:true;
  }

  deleteTodo (todo: Todo) {
    if (!confirm('are you sure you want to delete this todo?')) return;

    let index = this.todos.indexOf(todo);
    this.todos.splice(index, 1);

    console.log('---delete', this.todos);
  }

  addTodo() {
    if (this.todoDecs.match(/^(?=\s*\S).*$/)) {
      this.todos.push(new Todo(this.todoDecs));
    }
    this.todoDecs = '';

    console.log('---add', this.todos);
  }

  toggleStatus (todo: Todo) {
    todo.toggleStatus();
  }
}

export default TodoComponent;