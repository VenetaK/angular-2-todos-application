import Todo from './../models/Todo';

let todosMock: Array<Todo> = [
    new Todo('Finish the Todos task'),
    new Todo('Do the Pixi task'),
    new Todo('Another todo'),
    new Todo('Another, another todo')
];

export default todosMock;