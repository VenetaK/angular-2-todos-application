import { Injectable } from '@angular/core';
import Todo from './../models/Todo';
import TODOS from './../collections/TodosList';

@Injectable()
class TodosService {
    constructor () {

    }

    getTodos (): Promise<Array<Todo>> {
        return Promise.resolve(TODOS);
    }
}

export default TodosService;