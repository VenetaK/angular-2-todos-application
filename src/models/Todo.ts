class Todo {
    public desc: string;
    public id: number;
    public status: boolean;
    public inEdit: boolean;

    constructor(desc: string) {
        this.desc = desc;
        this.id = Date.now();
    };

    toggleStatus() {
        //if status is true - the todo is completed
        //if status is false - the todo is incomplete
        this.status = this.status?false:true;
    }
}

export default Todo;